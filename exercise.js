// Теоретичні питання

// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.

// Екранування - це спосіб використання спеціальних символів, як звичайних.Наприклад, нам потрібно
//   використати ' не як спеціальний символ одинарна кавичка, а як апостроф, в такому випадку нам
//   потрібно виконати екранування за допомогою спеціального символа \. Цк буде мати вигляд \'

// 2. Які засоби оголошення функцій ви знаєте?

// Існує два способи оголошення функції:
// -function declaration - розпочинається з ключового слова  function, далі назва функції і
// круглі дужки.Далі тіло функції у фігурних дужках.Функція викликається за допомогою імені
// та круглих дужок.

// function expression (функціональний вираз) - оголошується змінна, значенням якої є функція.
// Різниця між цими методами в тому, що function expression можна викликати тільки після її
// створення.

// 3. Що таке hoisting, як він працює для змінних та функцій?
 
// Hoisting - це процес доступу до змінних, оголошених за допомогою ключового слова var, та функцій
// до їх оголошення.
// У випадку з функціями Hoisting не працює при використанні стрілочних функцій, функціональних
// виразів, т.як це по факту оголошення змінної.
// У випадку використання змінних var до їх оголошення ми не отримаємо помилку, а отримаємо undefined,
//   т.як.JS ініціалізує їх значенням undefined.
//   У випадку змінних, що оголошені через let та const, при спробі використання їх до оголошення,
//   отримаємо помилку.
// Щоб уникнути помилок, потрібно оголошувати та ініціалізувати змінні до їх використання.




//     Завдання

/*Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. 
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або 
React.

Технічні вимоги:
Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть 
її наступним функціоналом:
При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти 
її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому 
регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. 
(наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() 
та getPassword() створеного об'єкта.*/

function createNewUser() {
  let firstName = prompt("Enter your name:");
  let lastName = prompt("Enter your surname:");

  let userBirthday = prompt("Enter your birthday dd.mm.yyyy:");

  const newUser = {
    get firstName() {
      return firstName;
    },
    get lastName() {
      return lastName;
    },
    get birthday() {
      return `${userBirthday}`;
    },
    set birthday(newBirthday) {
      this._birthday = newBirthday;
    },
    getLogin: function () {
      return `${this.firstName[0]}${this.lastName}`.toLowerCase();
    },
    set setFirstName(value) {
      firstName = value;
    },
    set setLastName(value) {
      lastName = value;
    },
    getAge: function () {
      let now = new Date();
      let userBirthdayDate = new Date(this._birthday);
      let userBirthdayAge = now.getFullYear() - userBirthdayDate.getFullYear();
      let nowMonth = now.getMonth();
      console.log(nowMonth);
      let userBirthdayMonth = userBirthdayDate.getMonth();
      console.log(userBirthdayMonth);
      let month = nowMonth - userBirthdayMonth;
      if (month < 0 || month===0 && now.getDate()<userBirthdayDate.getDate()) {
        return userBirthdayAge - 1;
      }
      return userBirthdayAge;
    },
    getPassword: function () {
      let firstLetterName = this.firstName[0].toUpperCase();
      let lastLNameLowerCase = this.lastName.toLowerCase();
      let userBirthdayYear = new Date(this._birthday).getFullYear();

      return firstLetterName + lastLNameLowerCase + userBirthdayYear;
    },
  };

  const birthdayParts = userBirthday.split(".");
  if (birthdayParts.length === 3) {
    newUser.birthday = `${birthdayParts[2]}-${birthdayParts[1]}-${birthdayParts[0]}`;
  }
  return newUser;
}

const newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser.firstName);
console.log(newUser.lastName);
console.log(newUser.birthday);
console.log(newUser.getAge());
console.log(newUser.getPassword());

